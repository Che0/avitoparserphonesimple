﻿using ParsersChe.Bot.ActionOverPage.EnumsPartPage;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.OleDb;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AvitoPhoneParser.BdLogic
{
    class DataParse
    {
        private static string connString;
        private static OleDbConnection database;
        static DataParse()
        {
            var bldr = new OleDbConnectionStringBuilder();
            bldr.DataSource = "dbp.accdb";
            bldr.Provider = "Microsoft.ACE.OLEDB.12.0";
            connString = bldr.ConnectionString;
            try
            {
                database = new OleDbConnection(connString);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Установите драйвер Microsoft.ACE.OLEDB.12.0"+Environment.NewLine+"http://www.microsoft.com/en-us/download/details.aspx?id=10910");
                throw ex;
            }
        }
        public static void Close()
        {
            database.Close();
        }
        public static void Open()
        {
            database.Open();
        }
        public static bool AddNewLinkIdNew(string link)
        {
            bool resultM;
            string idStr = Regex.Match(link, "\\d+$").Value;
            long code = Convert.ToInt64(idStr);

            //  Console.WriteLine(result);
            if (IsNew(code))
            {
                InsertLink(link);
                resultM = true;
            }
            else
            {
                resultM = false;
            }

            return resultM;
        }
        public static void Select()
        {
            string queryString = "Select LinkAvito from AvitoLink";
            OleDbCommand SQLQuery = new OleDbCommand(queryString, database);
            OleDbDataReader reader = SQLQuery.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine(reader[0]);
            }
        }

        public static bool IsNew(long code)
        {
            string queryString = "Select count(*) from AvitoAdCode where AvitoAdCode.Code=" + code;
            OleDbCommand SQLQuery = new OleDbCommand(queryString, database);
            int result = (int)SQLQuery.ExecuteScalar();
            if (result > 0) return false; else return true;
        }

        public static IEnumerable<string> LoadLinks()
        {
            string q = "Select LinkAvito from AvitoLink where AvitoLink.Used=false";
            IList<string> links = new List<String>();

            OleDbCommand SQLQuery = new OleDbCommand(q, database);
            OleDbDataReader reader = SQLQuery.ExecuteReader();
            while (reader.Read())
            {
                links.Add((string)reader[0]);
            }

            return links;
        }

        public static void ClearLinks()
        {
            string q = "delete from AvitoLink";
            string q2 = "delete from AvitoAdCode";
            string q3 = "delete from ParserResult";


            OleDbCommand SQLQuery = new OleDbCommand(q, database);
            OleDbCommand SQLQuery2 = new OleDbCommand(q2, database);
            OleDbCommand SQLQuery3 = new OleDbCommand(q3, database);

            int result = (int)SQLQuery.ExecuteNonQuery();
            SQLQuery2.ExecuteNonQuery();
            SQLQuery3.ExecuteNonQuery();

        }
        public static void UpdateUsedLink(string link)
        {
            string q = string.Format("UPDATE AvitoLink SET Used=true where LinkAvito='{0}'", link);
            OleDbCommand SQLQuery = new OleDbCommand(q, database);
            int result = (int)SQLQuery.ExecuteNonQuery();
        }

        public static void InsertLink(string link)
        {
            string idStr = Regex.Match(link, "\\d+$").Value;
            long code = Convert.ToInt64(idStr);

            string queryString = string.Format("INSERT INTO AvitoLink ( LinkAvito, Used )select '{0}',false", link);
            string query2 = string.Format("INSERT INTO AvitoAdCode ( Code )select {0}", code);
            OleDbCommand SQLQuery = new OleDbCommand(queryString, database);
            OleDbCommand SQLQuery2 = new OleDbCommand(query2, database);
            int result = (int)SQLQuery.ExecuteNonQuery();
            SQLQuery2.ExecuteNonQuery();
        }

        public static void InserData(Dictionary<PartsPage, IEnumerable<string>> result)
        {
            if (result != null)
            {
                Dictionary<PartsPage, IEnumerable<string>> dic = new Dictionary<PartsPage, IEnumerable<string>>();
                foreach (var item in result)
                {
                    PartsPage key = item.Key;
                    IEnumerable<string> val;
                    if (item.Value == null)
                    {
                        val = new List<string> { "" };
                    }
                    else val = item.Value;
                    dic.Add(key, val);
                }
                string phoneT = dic[PartsPage.Phone].First<string>().Replace(" ", "").Replace("+", "").Replace("-", "");
                long phone = 0;
                try
                {
                    if (!string.IsNullOrEmpty(phoneT)) phone = Convert.ToInt64(phoneT);
                }
                catch (Exception)
                {
                }
                string seller = dic[PartsPage.Seller].First<string>();
                string q = string.Format(@"INSERT INTO ParserResult (Phone,Seller )
                SELECT  {0},'{1}'", phone, seller);

                OleDbCommand SQLQuery = new OleDbCommand(q, database);
                SQLQuery.ExecuteNonQuery();
            }
        }
    }
}
