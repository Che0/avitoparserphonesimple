﻿using ParsersChe.Bot.ActionOverPage.EnumsPartPage;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace AvitoPhoneParser.BdLogic
{
    class DataParser2:IDisposable
    {
        private  string connString;
        private  OleDbConnection database;
        public DataParser2()
        {
            var bldr = new OleDbConnectionStringBuilder();
            bldr.DataSource = "dbp.accdb";
            bldr.Provider = "Microsoft.ACE.OLEDB.12.0";
            connString = bldr.ConnectionString;
            database = new OleDbConnection(connString);
        }

        public  void UpdateUsedLink(string link)
        {
            string q = string.Format("UPDATE AvitoLink SET Used=true where LinkAvito='{0}'", link);
            OleDbCommand SQLQuery = new OleDbCommand(q, database);
            int result = (int)SQLQuery.ExecuteNonQuery();
        }

        public  void InserData(Dictionary<PartsPage, IEnumerable<string>> result)
        {
            if (result != null)
            {
                Dictionary<PartsPage, IEnumerable<string>> dic = new Dictionary<PartsPage, IEnumerable<string>>();
                foreach (var item in result)
                {
                    PartsPage key = item.Key;
                    IEnumerable<string> val;
                    if (item.Value == null)
                    {
                        val = new List<string> { "" };
                    }
                    else val = item.Value;
                    dic.Add(key, val);
                }
                string phoneT = dic[PartsPage.Phone].First<string>().Replace(" ","").Replace("+","").Replace("-","");
                long phone = 0;
                try
                {
                    if (!string.IsNullOrEmpty(phoneT)) phone = Convert.ToInt64(phoneT);
                }
                catch (Exception)
                {
                }
                string seller = dic[PartsPage.Seller].First<string>();
                string costT = dic[PartsPage.Cost].First<string>();
                double cost = 0;
                try
                {
                    if (!string.IsNullOrEmpty(costT)) cost = Convert.ToDouble(costT);
                }
                catch (Exception)
                {
                }
                string city = dic[PartsPage.City].First<string>();
                string category = dic[PartsPage.Category].First<string>();
                string desc = dic[PartsPage.Body].First<string>();
                string q = string.Format(@"INSERT INTO ParserResult (Phone,Seller,Cost,City,Category,Body )
                SELECT  {0},'{1}', {2}, '{3}', '{4}','{5}'", phone, seller, cost, city, category, desc);

                OleDbCommand SQLQuery = new OleDbCommand(q, database);
                SQLQuery.ExecuteNonQuery();
            }
        }

        public  void Close()
        {
            database.Close();
        }
        public  void Open()
        {
            database.Open();
        }

        public void Dispose()
        {
            Close();
        }
    }
}
