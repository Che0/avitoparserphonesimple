﻿using AvitoPhoneParser.BdLogic;
using ParsersChe.WebClientParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AvitoPhoneParser
{
    public partial class AvitoPhonesLoader : Form
    {
        private AvitoClient ac;
        private IEnumerable<string> links;
        private bool usedForm = false;
        public AvitoPhonesLoader()
        {
            InitializeComponent();
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            textBoxLinksSection.Text = SettingsField.Default.Link;
            //DataParse.Open();
            //ac = new AvitoClient();
            //ac.GetPhone(new List<string> { "http://www.avito.ru/velikiy_ustyug/kvartiry/1-k_kvartira_38_m_33_aet._126768624" },
            //    new WebCl());
            //DataParse.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var t = Task.Factory.StartNew(() => Start());
            t.ContinueWith((x) => StartData());
        }

        private void Start()
        {
            if (!usedForm)
            {
                usedForm = true;
                links = null;

                if (textBoxLogin.Text != string.Empty && textBoxPassword.Text != string.Empty)
                {
                    ac = new AvitoClient(textBoxLogin.Text, textBoxPassword.Text, "proxy.txt");
                }
                else
                {
                    ac = new AvitoClient("proxy.txt");
                }

               //   ac = new AvitoClient();
                //    
                progressBarLink.Value = 0;
                ac.IsOrder = false;
                string item = textBoxLinksSection.Text;
                ac.IncLink = (() => IncLinks());
                progressBarLink.Maximum = ac.CountLink(item);
                label4.Text = "Программа работает";
                try
                {
                    DataParse.Open();
                    links = ac.ReadLinks(item);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    using (TextWriter tw = new StreamWriter("error.txt", true))
                    {
                        tw.WriteLine(Environment.NewLine);
                        tw.WriteLine(ex.Message);
                        tw.WriteLine(ex.StackTrace);
                    }
                }
                finally
                {
                    DataParse.Close();
                    label4.Text = "Сбор ссылок завершен";
                    progressBarLink.Value = progressBarLink.Maximum;
                    usedForm = false;
                }
            }
            else
            {
                MessageBox.Show("Программа занята выполнением другого процесса");
            }
        }

        private void IncLinks()
        {
            if (progressBarLink.Maximum > progressBarLink.Value)
                progressBarLink.Value++;
        }
        private void IncData()
        {
            if (progressBarData.Maximum > progressBarData.Value)
            {
                progressBarData.Value++;
                labelCounter.Text = progressBarData.Value.ToString();
            }
        }
        private void WriteResultPhones(IEnumerable<ulong> phones)
        {
            //HashSet<ulong> phonesUnique = new HashSet<ulong>(phones);
            //string path = "result.txt";
            //if (textBoxPathFolder == null) { textBoxPathFolder.Text = string.Empty; }
            //var maxCount = textBoxMaxLine.Text;
            //if (maxCount==string.Empty)
            //    using (TextWriter tw = new StreamWriter(textBoxPathFolder.Text + path))
            //    {
            //        foreach (var onePhone in phonesUnique)
            //        {
            //            tw.WriteLine(onePhone.ToString());
            //        }
            //    }
            //else 
            //{
            //    int max = Convert.ToInt32(maxCount);
            //    int currentCount = 0;
            //    int pathFile=1;
            //    foreach (var item in phonesUnique)
            //    {
            //        currentCount++;
            //        if (currentCount > max) 
            //        {
            //            currentCount = 0;
            //            pathFile++;
            //            path = "phones" + pathFile + ".txt";
            //        }
            //        using (TextWriter tw = new StreamWriter(textBoxPathFolder.Text + path, true))
            //        {
            //            tw.WriteLine(item.ToString());
            //        }
            //    }
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() => StartData());
        }

        private void StartData()
        {
            if (!usedForm)
            {
                usedForm = true;
                DataParse.Open();
                links = DataParse.LoadLinks();

                int countData = links.Count<string>();
                if (countData > 0)
                {
                    progressBarData.Value = 0;
                    if (ac == null)
                    {
                        if (textBoxLogin.Text != string.Empty && textBoxPassword.Text != string.Empty)
                        {
                            ac = new AvitoClient(textBoxLogin.Text, textBoxPassword.Text, "proxy.txt");
                        }
                        else
                        {
                            ac = new AvitoClient("proxy.txt");
                        }

                         //ac = new AvitoClient();
                            
                        ac.IsOrder = false;
                        string item = textBoxLinksSection.Text;
                    }
                    ac.IncData = (() => IncData());
                    label4.Text = "Сбор данных";

                    progressBarData.Maximum = countData;
                    progressBarLink.Maximum = countData;
                    ac.GetPhone(links);
                    //  WriteResultPhones(phones);
                    links = null;
                    progressBarData.Value = progressBarData.Maximum;
                    ac.Dispose();
                    label4.Text = "Работа завершенна";
                    Console.WriteLine("Finish");
                }
                DataParse.Close();
                usedForm = false;
            }
            else
            {
                MessageBox.Show("Программа занята выполнением другого процесса");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!usedForm)
            {
                usedForm = true;
                DataParse.Open();
                DataParse.ClearLinks();
                DataParse.Close();
                usedForm = false;
            }
            else
            {
                MessageBox.Show("Программа занята выполнением другого процесса");
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataParse.Select();
        }

        private void textBoxLinksSection_Validating(object sender, CancelEventArgs e)
        {
            //  if (string.IsNullOrEmpty(textBoxLinksSection.Text))
            //{
            //     e.Cancel= true;
            //     MessageBox.Show("Введите ссылку");
            //}
        }

        private void textBoxLinksSection_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void textBoxLinksSection_Validated(object sender, EventArgs e)
        {

        }

        private void textBoxLinksSection_TextChanged(object sender, EventArgs e)
        {
            SettingsField.Default.Link = textBoxLinksSection.Text;
            SettingsField.Default.Save();
        }

        private void AvitoPhonesLoader_Load(object sender, EventArgs e)
        {

        }
    }
}
