﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AvitoPhoneParser
{
    class Program
    {
       [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AvitoPhonesLoader());
       }
        private void Ma(string[] args)
        {
            if (args.Length == 2)
            {
                string pu = args[0];
                string pw = args[1];
                string[] linksSection = File.ReadAllLines("linksSection.txt");
                AvitoClient ac = new AvitoClient(pu, pw, "proxy.txt");
                foreach (var item in linksSection)
                {
                    IEnumerable<string> links = null;
                    try
                    {
                        links = ac.ReadLinks(item);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);
                        using (TextWriter tw = new StreamWriter("error.txt", true))
                        {
                            tw.WriteLine(Environment.NewLine);
                            tw.WriteLine(ex.Message);
                            tw.WriteLine(ex.StackTrace);
                        }
                    }
                    if (links != null)
                    {
                       // ac.GetPhone(links);
                    }
                    using (TextWriter tw = new StreamWriter("sectionPrepared.txt", true))
                    {
                        tw.WriteLine(item);
                    }

                }
            }
        }
    }
}
