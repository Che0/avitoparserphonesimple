﻿using AvitoPhoneParser.BdLogic;
using ParsersChe.Bot.ActionOverPage;
using ParsersChe.Bot.ActionOverPage.ContentPrepape;
using ParsersChe.Bot.ActionOverPage.ContentPrepape.Avito;
using ParsersChe.Bot.ActionOverPage.EnumsPartPage;
using ParsersChe.Bot.ContentPrepape.Avito;
using ParsersChe.WebClientParser;
using ParsersChe.WebClientParser.Proxy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoPhoneParser
{
    class AvitoClient : IDisposable
    {
        private IHttpWeb webCl;
        public string PathToFolder { get; set; }
        public int maxLinesOnFile;
        private string pathToFile;
        public bool IsOrder { get; set; }
        public Action IncLink { get; set; }
        public Action IncData { get; set; }

        public AvitoClient(string userProxy, string passwordProxy, string pathToProxy)
        {
            ProxyCollectionSingl.ProxyPass = passwordProxy;
            ProxyCollectionSingl.ProxyPath = pathToProxy;
            ProxyCollectionSingl.ProxyUser = userProxy;
         //   var pcs = new ProxyCollSeparate(userProxy, passwordProxy, pathToProxy);
            webCl = new WebClProxy();

        }
        public AvitoClient(string pathToProxy)
        {
            ProxyCollectionSingl.ProxyPath = pathToProxy;
            webCl = new WebClProxy();
            //var pcs = new ProxyCollSeparate(pathToProxy);
            //webCl = new WebClSepNewProxy(pcs);
        }
        public AvitoClient()
        {
            webCl = new WebCl();
        }

        public int CountLink(string linkSection)
        {
            int countLink = 0;
            string url = linkSection;
            ParserPage parser = new SimpleParserPage
              (url, new List<IPrepareContent> {
                    new AvitoCountLinks()
                   }, webCl
              );
            parser.RunActions();
            var result = parser.ResultsParsing;
            IEnumerable<string> resultSend = result[PartsPage.CountLink];
            if (resultSend != null)
            {
                string countStr = resultSend.First<string>();
                if (!string.IsNullOrEmpty(countStr))
                {
                    countLink = Convert.ToInt32(countStr);
                }
                Console.WriteLine(countLink);
            }
            return countLink;
        }

        public IEnumerable<string> ReadLinks(string linkSection)
        {
            AvitoLoadLinksBeforeRepeat loadLinkModule;
            loadLinkModule = new AvitoLoadLinksBeforeRepeat(webCl, 999999, (x) => DataParse.IsNew(x));
            loadLinkModule.IncLink = IncLink;
            loadLinkModule.AddData = ((x) => DataParse.InsertLink(x));
            string url = linkSection;
            ParserPage parser = new SimpleParserPage
              (url, new List<IPrepareContent> {
                    loadLinkModule
                           }, webCl
              );
            parser.RunActions();
            var result = parser.ResultsParsing;
            var links = result[PartsPage.LinkOnAd];
            return links;
        }

        public void GetData3Task(IEnumerable<string> links) 
        {
            ProxyCollSeparate p1;
           
            if (!string.IsNullOrEmpty(ProxyCollectionSingl.ProxyUser))
            {
             p1 = new ProxyCollSeparate(ProxyCollectionSingl.ProxyUser,ProxyCollectionSingl.ProxyPass,"proxy.txt");
            }
            else
            {
                 p1 = new ProxyCollSeparate("proxy.txt");
            }

            WebClSeperateProxy wc1 = new WebClSeperateProxy(p1);
            IList<Task> tasks = new List<Task>
            {
                Task.Factory.StartNew(()=>GetPhone(links,wc1)),

            };
            Task.WaitAll(tasks.ToArray<Task>());
        }
        public IEnumerable<ulong> GetPhone(IEnumerable<string> links)
        {

            // File.WriteAllText("phones.txt", "");
            //DataParser2 dp2 = new DataParser2();
            //dp2.Open();
            IList<ulong> phonesList = new List<ulong>();
            foreach (var item in links)
            {
                try
                {
                    string url = item;
                    ParserPage parser = new SimpleParserPage
                      (url, new List<IPrepareContent> {
                    new AvitoPhones(webCl),
                    new AvitoSeller()
                   }, webCl
                      );
                    parser.RunActions();
                    var result = parser.ResultsParsing;
                    if (result != null)
                    {
                        IEnumerable<string> resultSend = result[PartsPage.Phone];
                        if (resultSend != null)
                        {
                            if (IncData != null) IncData();
                            DataParse.InserData(result);
                              WriteLine(result);
                        }
                    }
                    DataParse.UpdateUsedLink(item);

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    using (TextWriter tw = new StreamWriter("error.txt", true))
                    {
                        tw.WriteLine(Environment.NewLine);
                        tw.WriteLine(ex.Message);
                        tw.WriteLine(ex.StackTrace);
                    }
                }
                //     finally { DataParse.Close(); }
            }
            if (IsOrder)
            {
                IEnumerable<ulong> psh = phonesList.OrderBy(x => x);
                return psh;
            }
            return phonesList;
        }
        public IEnumerable<ulong> GetPhone(IEnumerable<string> links, IHttpWeb webClcurrent)
        {

           // File.WriteAllText("phones.txt", "");
            //DataParser2 dp2 = new DataParser2();
            //dp2.Open();
            IList<ulong> phonesList = new List<ulong>();
            foreach (var item in links)
            {
                try
                {
                    string url = item;
                    ParserPage parser = new SimpleParserPage
                      (url, new List<IPrepareContent> {
                    new AvitoPhones(webClcurrent)
                   }, webCl
                      );
                    parser.RunActions();
                    var result = parser.ResultsParsing;
                    if (result != null)
                    {
                        IEnumerable<string> resultSend = result[PartsPage.Phone];
                        if (resultSend != null)
                        {
                            if (IncData != null) IncData();
                            DataParse.InserData(result);
                          //  WriteLine(result);
                        }
                    }
                    DataParse.UpdateUsedLink(item);

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    using (TextWriter tw = new StreamWriter("error.txt", true))
                    {
                        tw.WriteLine(Environment.NewLine);
                        tw.WriteLine(ex.Message);
                        tw.WriteLine(ex.StackTrace);
                    }
                }
           //     finally { DataParse.Close(); }
            }
            if (IsOrder)
            {
                IEnumerable<ulong> psh = phonesList.OrderBy(x => x);
                return psh;
            }
            return phonesList;
        }


        private void WriteLine(Dictionary<PartsPage, IEnumerable<string>> result)
        {
            IList<IEnumerable<string>> fields = new List<IEnumerable<string>>()
            {
                result[PartsPage.Cost],
                result[PartsPage.Seller],
                result[PartsPage.Phone],
                result[PartsPage.City],
                result[PartsPage.Category],
                result[PartsPage.Body]
            };
            //StringBuilder sb = new StringBuilder("Cost;Seller;Phone;City;Category;BodyAd"+Environment.NewLine);
            StringBuilder line = new StringBuilder();
            string sep = "";
            foreach (var item in fields)
            {
                line.Append(sep);
                if (item != null)
                {
                    string res = item.First<string>().Replace("\"", "");
                    line.Append("\""+res+"\"");
                }
                else
                {
                    line.Append("\"\"");
                }
                if (sep.Equals("")) sep = ";";
            }
           // line.Append(Environment.NewLine);
            using (TextWriter tw = new StreamWriter("result.txt", true))
            {
                tw.WriteLine(line.ToString());
            }
        }
        public void Dispose()
        {
            ProxyCollectionSingl.Instance.Dispose();
        }
    }
}
